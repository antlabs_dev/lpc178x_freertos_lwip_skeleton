/**
 * @file 	main.c
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.1
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Applications entry point and basic hardware setup
 */

/*
 * Application-specific includes
 */
#include "AppConfig.h"
#include "Utils/network_init.h"

void soft_reset(void);

static void setup_main_hw(void)
{
	UART_CFG_Type UARTConfigStruct;

	PINSEL_ConfigPin (0, 2, 1);
	PINSEL_ConfigPin (0, 3, 1);
	UART_ConfigStructInit(&UARTConfigStruct);
	// Re-configure baudrate to 115200bps
	UARTConfigStruct.Baud_rate = 115200u;

	// Initialize DEBUG_UART_PORT peripheral with given to corresponding parameter
	UART_Init(LPC_UART0, &UARTConfigStruct);

	// Enable UART Transmit
	UART_TxCmd(LPC_UART0, ENABLE);

#ifdef APPCONFIG_DO_YOU_LIKE_DOGS
	/* Enable watchdog*/
	//TODO implement
#endif
}

/**
 * Program entry point 
 */
int main(void)
{
	//SDRAMInit();
	/*
	 * Reallocate vector table if needed, i.e. started from not 0 address
	 */
	REALLOCATE_VECTOR_TABLE;

	setup_main_hw();

	/* First record in log also initialize it */
	syslog_write(LOG_INFORMATION, (signed portCHAR *)"main()", "Started. CPU clock = %lu MHz, SDRAM clock = %lu",
				SystemCoreClock / 1000000u, EMCClock / 1000000u);

	/* Start software RTC */
	soft_rtc_start();

	static struct s_network_config net;
	net.ip_addr = "192.168.1.221";
	net.gateway = "192.168.1.1";
	net.netmask = "255.255.255.0";
	net.dns = "192.168.1.1";
	//net.use_dhcp = 1; // set to 1 to enable DHCP. Or set to 0 and fill the struct manually
	tcpip_init(network_init, &net);

	/* Syncronize the time with internet using RFC868 protocol and set timezone to EEST (UTC+3) */
	sync_time_rfc868(NULL, LOCAL_TIMEZONE);

	/* Start Telnet daemon */
	telnetd_start();

	vTaskStartScheduler();

	/* 
	 * Will only get here if there was insufficient memory to create the idle task. 
	 */
	TSPRINTF("Scheduler not started\n");

	return 1;
}

void vApplicationStackOverflowHook( xTaskHandle pxTask, signed char *pcTaskName )
{
	/* Bad thing =( Print some useful info and reboot */
	TSPRINTF("Stack overflow in %s\n", pcTaskName);
	/* The is a chance that syslog is still working */
	syslog_write(LOG_ERROR, pcTaskName, "Stack overflow");
	soft_reset();
}

/**
 * Reset CPU using watchdog
 */
void soft_reset(void)
{
	__disable_irq();
	__disable_fault_irq();
	volatile uint_fast8_t fuck_optimization = 0;
	while(1) // wait until wdt overflows
	{
		fuck_optimization++; // ...and bark after WDT_RESET_TIMEOUT
	}
}

void vApplicationIdleHook(void)
{
	//TODO feed the dog
}


void vApplicationTickHook(void)
{

}

void HardFault_Handler(void)
{
	/* Very bad thing =( Print some useful info and reboot */
	TSPRINTF("HardFault CFSR = %X  BFAR = %X\n", SCB->CFSR, SCB->BFAR);
	soft_reset();
}

void NMI_Handler(void)
{
	TSPRINTF("NMI\n");
	soft_reset();
}

void MemManage_Handler(void)
{
	TSPRINTF("MemManage\n");
	soft_reset();
}

void BusFault_Handler(void)
{
	TSPRINTF("BusFault\n");
	soft_reset();
}

void UsageFault_Handler(void)
{
	TSPRINTF("UsageFault\n");
	soft_reset();
}


void DebugMon_Handler(void)
{
	TSPRINTF("DebugMon\n");
	soft_reset();
}

