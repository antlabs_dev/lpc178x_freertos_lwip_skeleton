/**
 * @file 	soft_rtc.h
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Software RTC based on FreeRTOS task with highest priority
 */

#ifndef SOFTRTC_H_
#define SOFTRTC_H_

#include "AppConfig.h"
#include <time.h>

/**
 * Enumerated timezones are used as index in array of timezone offsets
 */
enum TimeZoneNumber {
	UTC_M_1200,
	UTC_M_1100,
	UTC_M_1000,
	UTC_M_0930,
	UTC_M_0900,
	UTC_M_0800,
	UTC_M_0700,
	UTC_M_0600,
	UTC_M_0500,
	UTC_M_0430,
	UTC_M_0400,
	UTC_M_0330,
	UTC_M_0300,
	UTC_M_0200,
	UTC_M_0100,
	UTC,
	UTC_P_0100,
	UTC_P_0200,
	UTC_P_0300,
	UTC_P_0330,
	UTC_P_0400,
	UTC_P_0430,
	UTC_P_0500,
	UTC_P_0530,
	UTC_P_0545,
	UTC_P_0600,
	UTC_P_0630,
	UTC_P_0700,
	UTC_P_0800,
	UTC_P_0845,
	UTC_P_0900,
	UTC_P_0930,
	UTC_P_1000,
	UTC_P_1030,
	UTC_P_1100,
	UTC_P_1130,
	UTC_P_1200,
	UTC_P_1245,
	UTC_P_1300,
	UTC_P_1400
};

/**
 * Enumed type of time counter: Posix (seconds since 1970) ao NTP (seconds from 1900)
 */
enum GetTimeCounterType {
	POSIXTIME_T,
	NTPTIME_T
};

/**
 * Initialize the task - 1 second increment counter
 */
uint_fast8_t soft_rtc_start(void);

/**
 * Public interface - return unix or ntp time
 * @param type - type of the time 	UNIXTIME_T or NTPTIME_T
 * @return current unixtime counter
 */
time_t get_time_counter(int type);

/**
 * Public interface - set current timezone
 * @param type - timezone name, see enum TimeZoneNumber
 */
void set_timezone(int type);

/**
 * Public interface to set current time in unix format
 * @param type - type of the time 	UNIXTIME_T or NTPTIME_T
 * @param newtime - time_t with new time
 */
void set_time_counter(int type, time_t newtime);

/**
 * Public interface - return time_t since start up
 */
time_t get_uptime(void);

#endif /* SOFTRTC_H_ */
