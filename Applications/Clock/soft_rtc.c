/**
 * @file 	coft_rtc.h
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Software RTC based on FreeRTOS task with highest priority
 */

#include "soft_rtc.h"

static time_t posixtime = 0, uptime = 0; //!< Counters
static int32_t timezone_offset = 0; //!< Current time zone (offset is seconds)
static const time_t ntp_posix_correction = 2208988800u; //!< Difference between NTP and Posiz time in seconds

static void soft_rtc_task(void *parameters);

/**
* UTC +/- correction in seconds, see enum TimeZoneNumber in header
*/
static const int32_t timezone[] = {
	-43200,
	-39600,
	-36000,
	-34200,
	-32400,
	-28800,
	-25200,
	-21600,
	-18000,
	-16200,
	-14400,
	-12600,
	-10800,
	-7200,
	-3600,
	0,
	3600,
	7200,
	10800,
	12600,
	14400,
	16200,
	18000,
	19800,
	20700,
	21600,
	23400,
	25200,
	28800,
	31500,
	32400,
	34200,
	36000,
	37800,
	39600,
	41400,
	43200,
	45900,
	46800,
	50400
};

uint_fast8_t soft_rtc_start(void)
{
	portBASE_TYPE res = xTaskCreate(
			soft_rtc_task,
			(signed portCHAR *)"soft rtc",
			50u,
			NULL,
			configSOFTRTC_TASK_PRIORITY,
			NULL);
	if(res != pdPASS)
	{
		return 0;
	}
	return 1u;
}

/**
 * The RTC task itself. Increments counters
 * @param parameters - supposed to a task parameters, but not used here
 */
static void soft_rtc_task(void *parameters)
{
	portTickType xNextWakeTime = xTaskGetTickCount();
	while(1)
	{
		posixtime++;
		uptime++;
		vTaskDelayUntil(&xNextWakeTime, 1000 / portTICK_RATE_MS);
	}
}

time_t get_time_counter(int type)
{
	switch(type)
	{
	/* Here we have some signed/unsigned magic. Don't panic, it works */
	case POSIXTIME_T: return posixtime + timezone_offset;
	case NTPTIME_T: return(posixtime + timezone_offset + ntp_posix_correction);
	default: return 0;
	}
}

void set_timezone(int type)
{
	if(type > 0 && type < (sizeof(timezone) / sizeof(timezone[0])))
	{	/*Valid type */
		timezone_offset = timezone[type];
	}
}

void set_time_counter(int type, time_t newtime)
{
	switch(type)
	{
	case POSIXTIME_T:
		posixtime = newtime;
		break;
	case NTPTIME_T:
		posixtime = newtime - ntp_posix_correction;
		break;
	default:
		return;
	}
}

time_t get_uptime(void)
{
	return uptime;
}
