/**
 * @file 	rfc868_time_sync.c
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Synchronize time with internet usin RFC868 protocol
 */

#include "rfc868_time_sync.h"
#include "Utils/network_init.h"

static const char * const timeservers[20] = {
		"64.90.182.55",
		"96.47.67.105",
		"206.246.122.250",
		"64.236.96.53",
		"216.119.63.113",
		"64.250.177.145",
		"208.66.175.36",
		"173.14.55.9",
		"64.113.32.5",
		"66.219.116.140",
		"216.229.0.179",
		"24.56.178.140",
		"128.138.140.44",
		"128.138.188.172",
		"198.60.73.8",
		"64.250.229.100",
		"207.200.81.113",
		"69.25.96.13",
		"216.171.124.36",
		"64.147.116.229"
};

struct s_sync_time_rfc868_task_parameters {
	const char *priority_server;
	int timezone;
};

static void sync_time_rfc868_task(void *parameters);

uint_fast8_t sync_time_rfc868(const char *priority_server, int timezone)
{
	static struct s_sync_time_rfc868_task_parameters params;
	params.priority_server = priority_server;
	params.timezone = timezone;

	portBASE_TYPE res = xTaskCreate(
			sync_time_rfc868_task,
			(signed portCHAR *)"rfc868 time",
			400u,
			(void *)&params,
			1,
			NULL);
	if(res != pdPASS)
	{
		syslog_write(LOG_ERROR, "sync_time_rfc868()", "Cannot create rfc868 timesync task");
		return 0;
	}
	return 1;
}

static void sync_time_rfc868_task(void *parameters)
{
	struct s_sync_time_rfc868_task_parameters params = *((struct s_sync_time_rfc868_task_parameters *)parameters);

	START_TASK_AFTER_INTERFACE_UP();

	const uint_fast32_t timeout = 20000u; // 20 seconds blocking wait
	int32_t socket = socket(AF_INET, SOCK_STREAM, 0);
	if(socket >= 0)
	{
		setsockopt(socket, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof timeout);

		struct sockaddr_in server;

		uint_fast8_t i = 0, ok = 0, done = 0;
		time_t tm = 0;
		uint8_t buff[4] = {0};

		memset((void *)&server, 0, sizeof server);

		server.sin_family = AF_INET;
		server.sin_port = htons(37);

		const char *syncronized_with = NULL;

		if(NULL != params.priority_server)
		{
			/* Validate string host whether it is IP address or domain name */
			ip_addr_t temp;
			uint8_t use_dns = !inet_aton(params.priority_server, &temp);

			if(use_dns)
			{
				struct hostent *hp_result, h_hp;
				char buf[64] = {0};
				int err = -1;

				if(gethostbyname_r(params.priority_server, &h_hp, buf, sizeof buf, &hp_result, &err) == 0)
				{
					syslog_write(LOG_INFORMATION, pcTaskGetTaskName(NULL), "Resolved %s is %s", hp_result->h_name,
							inet_ntoa(*(struct in_addr *)(hp_result->h_addr_list[0])));
					server.sin_addr = *(struct in_addr *)(hp_result->h_addr_list[0]);
				}
				else
				{
					syslog_write(LOG_INFORMATION, pcTaskGetTaskName(NULL), "Cannot resolve %s, error %d", hp_result->h_name, err);
					done = 1u;
					ok = 0;
				}
			}
			else
			{
				server.sin_addr.s_addr = inet_addr(params.priority_server);
			}

			if(connect(socket, (struct sockaddr *)&server, sizeof server) >= 0)
			{
				if(recv(socket, buff, sizeof buff, 0) == sizeof buff)
				{
					tm = buff[3] + (buff[2] << 8u) + (buff[1] << 16u) + (buff[0] << 24u);
					ok = 1;
					done = 1;
					syncronized_with = params.priority_server;
				}
			}
			else
			{
				syslog_write(LOG_ERROR, pcTaskGetTaskName(NULL), "Cannot connect to %s:%u priority server", \
									inet_ntoa(server.sin_addr.s_addr), ntohs(server.sin_port));
			}
		}

		while(!done)
		{
			vTaskDelay(2000u / portTICK_RATE_MS);

			shutdown(socket, SHUT_RDWR);
			close(socket);
			socket = socket(AF_INET, SOCK_STREAM, 0);
			setsockopt(socket, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof timeout);

			memset((void *)&server, 0, sizeof server);

			server.sin_family = AF_INET;
			server.sin_port = htons(37u);
			server.sin_addr.s_addr = inet_addr(timeservers[i]);

			if(connect(socket, (struct sockaddr *)&server, sizeof server) < 0)
			{
				syslog_write(LOG_ERROR, pcTaskGetTaskName(NULL), "Cannot connect to %s:%u", \
													inet_ntoa(server.sin_addr.s_addr), ntohs(server.sin_port));
			}
			else
			{
				if(recv(socket, buff, sizeof buff, 0) == sizeof buff)
				{
					tm = buff[3] + (buff[2] << 8u) + (buff[1] << 16u) + (buff[0] << 24u);
					ok = 1;
					done = 1;
					syncronized_with = timeservers[i];
				}

			}
			if(i++ >= 19u)
			{
				done = 1u;
			}
		}

		if(ok)
		{
			set_time_counter(NTPTIME_T, tm);
			set_timezone(params.timezone);
			syslog_write(LOG_INFORMATION, pcTaskGetTaskName(NULL), "Time has been synchronized with %s", syncronized_with);
		}
		else
		{
			syslog_write(LOG_ERROR, pcTaskGetTaskName(NULL), "Cannot syncronize the time");
		}
		close(socket);
	}
	vTaskDelete(NULL);
}
