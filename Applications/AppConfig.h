#ifndef APPCONFIG_H_
#define APPCONFIG_H_

/*
 * Standard includes
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

/*
 * Peripheral library includes
 */
#include "lpc177x_8x_uart.h"
#include "lpc177x_8x_pinsel.h"
#include "lpc177x_8x_gpio.h"
#include "lpc177x_8x_nvic.h"
#include "lpc177x_8x_wwdt.h"

/*
 * FreeRTOS includes
 */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "timers.h"
#include "FreeRTOS_CLI.h"

/*
 * lwIP core includes
 */
#include "lwip/opt.h"
#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

/*
 * Utils includes
 */

#include "Clock/soft_rtc.h"
#include "Clock/format_time.h"
#include "Clock/rfc868_time_sync.h"
#include "Utils/syslog.h"
#include "Telnet/telnetd.h"


/*
 * External SDRAM
 */
#include "sdram_k4s561632j.h"

/*
 * Macros needed to place data in specified RAM region
 */
#include "cr_section_macros.h"

/*
 * CMSIS include
 */
#if defined (__USE_CMSIS)
	#include "system_LPC177x_8x.h"
#endif

/**
 * Thread-safe printf to default UART. See printf_redirect.c
 */
#define TSPRINTF(...) taskENTER_CRITICAL(); \
					printf(__VA_ARGS__); \
					taskEXIT_CRITICAL()

/**
 * Thread-safe printf only in DEBUG configuration
 */
#ifdef DEBUG
	#define DTSPRINTF TSPRINTF
#else
	#define DTSPRINTF(format, args...) ((void)0)
#endif

/**
 * WDT configuration
 */
#define APPCONFIG_DO_YOU_LIKE_DOGS //<! define to enable watchdog
#ifdef APPCONFIG_DO_YOU_LIKE_DOGS
	#define WDT_RESET_TIMEOUT (2u * 1000u * 1000u) // 2 seconds
#endif

#define LOCAL_TIMEZONE UTC_P_0300 //!< timezone used when you syncronize the time @see enum TimeZoneNumber in soft_rtc.h

/**
 * Setup application only when it started from not 0 address (i.e. bootloader is used)
 */
//#define USE_BOOTLOADER
#ifdef USE_BOOTLOADER
	#define APPLICATION_START_ADDRESS 0x4000
	#define REALLOCATE_VECTOR_TABLE (NVIC_SetVTOR(APPLICATION_START_ADDRESS))
#else
	#define REALLOCATE_VECTOR_TABLE ((void)0)
#endif

#endif /* APPCONFIG_H_ */
