/**
 * @file 	syslog.h
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.1
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Embedded SysLog module. Uses UART or whatever you want as output device.
 */

#ifndef SYSLOG_H_
#define SYSLOG_H_

#include "AppConfig.h"
//#include "usb_cdc_vcom.h"
#include <stdarg.h>

#define SYSLOG_MAXLINE (configMAX_TASK_NAME_LEN + 128u)
#define SYSLOG_MINLINE 16
#define SYSLOG_QUEUE_SIZE 1
#define SYSLOG_QUEUE_BLOCK_TIME 2

/**
 * enum type of log message
 */
typedef enum {
	LOG_ERROR,
	LOG_WARNING,
	LOG_INFORMATION,
	LOG_FATAL
} SysLogRecordType;

uint_fast8_t syslog_write(SysLogRecordType type, const signed char * const threadname, const char *fmt, ...);

#endif /* SYSLOG_H_ */
