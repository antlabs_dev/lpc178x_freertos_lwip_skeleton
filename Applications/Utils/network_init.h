/**
 * @file 	network_init.h
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.1
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Start LwIP daemon
 */

#ifndef NETWORK_APPS_H_
#define NETWORK_APPS_H_

#include "AppConfig.h"

#include "lwip/tcpip.h"
#include "netif/etharp.h"
#include "lwip/netif.h"
#include "lwip/dhcp.h"

/** Structure with network configuration */
struct s_network_config {
	const char *ip_addr; //!< local IP address as string, i.e. 192.168.1.100 or something like this
	const char *gateway; //!< default gateway
	const char *netmask; //!< local subnet mask
	const char *dns;	 //!< primary DNS server. If not specified the default 208.67.222.222 = resolver1.opendns.com will be used
	uint8_t use_dhcp;	 //!< set to 1 if you want DHCP
};

/**
 * Initialize the network
 * @param parameters - pointer to struct s_network_config
 */
void network_init(void *parameters);

/**
 * Check if the network interface is up
 * @return 1 if yes, 0 otherwise
 */
uint_fast8_t is_netiface_up(void);

/**
 * Macro tests if the LwIP stack is already configured. Use it in network applications before any operation
 */
#define START_TASK_AFTER_INTERFACE_UP() \
		while(!is_netiface_up())\
		{\
			vTaskDelay(1080u / portTICK_RATE_MS);\
		}


#endif /* NETWORK_APPS_H_ */
