/**
 * @file 	syslog.c
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.1
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Embedded SysLog module. Uses UART or whatever you want as output device.
 */

#include "syslog.h"
//#include "ff.h"

/**
 * Structure represents SysLog worker thread parameters
 */
struct log_writer_task_parameters {
	xQueueHandle queue;			//!< queue for receiveing messages
	struct s_syslog_devices {
		uint8_t uart0:1;        //!< List of devices where SysLog have to write data
		uint8_t uart3:1;
		uint8_t usb_vcom:1;
		uint8_t sdcard:1;
	} devs;
};

static void write_task(void *parameters);
static size_t format_string(char *buffer, size_t size, SysLogRecordType type, const char * const threadname, const char *fmt, va_list args);

uint_fast8_t syslog_write(SysLogRecordType type, const signed char * const threadname, const char *fmt, ...)
{
	if(!fmt)
	{
		return 0;
	}

	static xQueueHandle syslog_queue = NULL;
	static xSemaphoreHandle mutex = NULL;

	static char string[SYSLOG_MAXLINE] = {0};

	if(!syslog_queue)
	{
		syslog_queue = xQueueCreate(SYSLOG_QUEUE_SIZE, SYSLOG_MAXLINE);
		if(!syslog_queue)
		{
			return 0;
		}
		static struct log_writer_task_parameters params;
		params.queue = syslog_queue;
		params.devs.uart0 = 1u;
		params.devs.uart3 = 0;
		params.devs.usb_vcom = 0;
		params.devs.sdcard = 0;
		if(xTaskCreate(write_task, (signed portCHAR *)"log write", \
				SYSLOG_MAXLINE + 290u, (void *)&params, configSYSLOG_WRITE_TASK_PRIORITY, NULL) != pdPASS)
		{
			return 0;
		}
		mutex = xSemaphoreCreateRecursiveMutex();
		if(!mutex)
		{
			return 0;
		}
	}

	if(xSemaphoreTakeRecursive(mutex, (SYSLOG_QUEUE_BLOCK_TIME * 1000u) / portTICK_RATE_MS) != pdPASS)
	{
		return 0;
	}
	memset(string, 0, sizeof string);
	va_list args;
	va_start(args, fmt);
	format_string(string, sizeof string, type, (const char * const)threadname, fmt, args);
	va_end(args);

	portBASE_TYPE res = xQueueSend(syslog_queue, string, (SYSLOG_QUEUE_BLOCK_TIME * 1000u) / portTICK_RATE_MS);
	xSemaphoreGiveRecursive(mutex);
	if(res != pdPASS)
	{
			return 0;
	}
	return 1u;
}

static size_t format_string(char *buffer, size_t size, SysLogRecordType type, const char * const threadname, const char *fmt, va_list args)
{
	if(!buffer || size < SYSLOG_MINLINE || size > SYSLOG_MAXLINE)
	{
		return 0;
	}
	size_t result = 0;
	const char *type_fmt = NULL, *threadname_fmt = NULL;
	switch(type)
	{
	case LOG_ERROR:
		type_fmt = "|ERROR|";
		break;
	case LOG_WARNING:
		type_fmt = "|WARNING|";
		break;
	case LOG_INFORMATION:
		type_fmt = "|INFORMATION|";
		break;
	case LOG_FATAL:
		type_fmt = "|FATAL ERROR|";
		break;
	default:
		break;
	}
	threadname_fmt = threadname ? threadname : "(NULL)";
	strcat(buffer, "[");
	if(get_string_time(DEFAULT_TIME_FORMAT, buffer + 1u, size - 1u))
	{
		strcat(buffer, "]");
		if(strlen(threadname_fmt) + strlen(buffer) + 10u + strlen(type_fmt) < size)
		{
			strcat(buffer, " ");
			strcat(buffer, threadname_fmt);
			strcat(buffer, " ");
			strcat(buffer, type_fmt);
			strcat(buffer, " ");
			size_t i = strlen(buffer);
			if(i + strlen(fmt) < size)
			{
				size_t bytes = vsnprintf(buffer + i, size, fmt, args);
				if(bytes < size)
				{
					result = bytes;
				}
			}
		}
	}
	return result;
}

static void write_task(void *parameters)
{
	struct log_writer_task_parameters *params = (struct log_writer_task_parameters *)parameters;

	xQueueHandle syslog_queue = params->queue;
	char buff[SYSLOG_MAXLINE + 2u] = {0};
	const char * const endl = "\r\n";
/*	FIL fp;
	FATFS ffs;
	uint8_t sdcard_file_open = 0;
	const char * const dir =	  "0:logs";
	const char * const fullpath = "0:logs/freertos.log";
*/
	while(1)
	{
		memset((void *)buff, 0, sizeof buff);

		xQueueReceive(syslog_queue, buff, portMAX_DELAY);

		strcat(buff, endl);

		//DTSPRINTF(buff);

		if(params->devs.uart0)
		{
			/* Since this is 0-priority task operation can be preempted and another task can use UART0. Be careful */
			UART_Send((LPC_UART_TypeDef *)LPC_UART0, buff, strlen(buff), BLOCKING);
		}
		if(params->devs.uart3)
		{
			/* Since this is 0-priority task operation can be preempted and another task can use UART3. Be careful */
			UART_Send((LPC_UART_TypeDef *)LPC_UART3, buff, strlen(buff), BLOCKING);
		}
	/*	if(params->devs.usb_vcom)
		{
			usb_vcom_send(buff, strlen(buff), 1000u / portTICK_RATE_MS);
		}*/
	/*	if(params->devs.sdcard)
		{
			size_t len_written = 0;
			if(!sdcard_file_open)
			{
				f_mount(0, &ffs);
				f_mkdir(dir);
				if(f_open(&fp, fullpath, FA_WRITE | FA_OPEN_ALWAYS | FA_READ) == FR_OK)
				{
					sdcard_file_open = 1u;
				}
			}
			if(sdcard_file_open)
			{
				f_lseek(&fp, f_size(&fp));
				f_write(&fp, buff, strlen(buff), &len_written);
				f_sync(&fp);
			}
		}*/
	}
}

