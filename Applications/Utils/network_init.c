/**
 * @file 	network_init.c
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.1
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Start LwIP daemon
 */

#include "network_init.h"

extern err_t ethernetif_init(struct netif *xNetIf);

static uint8_t iface_up = 0;

static void iface_callback(struct netif *iface)
{
	char ip[16] = {0}, gw[16] = {0}, mask[16] = {0};
	strcpy(ip, inet_ntoa(iface->ip_addr));
	strcpy(gw, inet_ntoa(iface->gw));
	strcpy(mask, inet_ntoa(iface->netmask));

	syslog_write(LOG_INFORMATION, pcTaskGetTaskName(NULL), "Network interface is up, ip = %s, gw = %s, mask = %s", \
			 ip, gw, mask);
	iface_up = 1u;
}

uint_fast8_t is_netiface_up(void)
{
	return iface_up;
}

/* Called from the TCP/IP thread */
void network_init(void *parameters)
{
	struct s_network_config cfg = *((struct s_network_config *)parameters);

	ip_addr_t ip_address, netmask, gateway, dns;
	static struct netif net_iface;

	/* Set up the network interface. */
	ip_addr_set_zero(&gateway);
	ip_addr_set_zero(&ip_address);
	ip_addr_set_zero(&netmask);

	uint32_t serial[4] = {3464378u, 346225u,75278765u, 43565432u};
	//iap_read_serial(serial);
	net_iface.hwaddr[0] = (serial[0] ^ serial[1] ^ (serial[2] >> 8u)) & 0xFC; // XXX 0 and 1 bits are important, see wikipedia and don't spend hours for it again, stupid
	net_iface.hwaddr[1] = (serial[1] ^ serial[2] ^ (serial[3] >> 16u)) & 0xFF;
	net_iface.hwaddr[2] = (serial[2] ^ serial[3] ^ (serial[1] >> 24u)) & 0xFF;
	net_iface.hwaddr[3] = (serial[3] ^ serial[0] ^ (serial[2] >> 8u)) & 0xFF;
	net_iface.hwaddr[4] = (serial[3] ^ serial[1] ^ (serial[0] >> 16u)) & 0xFF;
	net_iface.hwaddr[5] = (serial[2] ^ serial[0] ^ (serial[1] >> 24u)) & 0xFF;

	if(!cfg.use_dhcp)
	{
		if(!inet_aton(cfg.ip_addr, &ip_address))
		{
			syslog_write(LOG_ERROR, pcTaskGetTaskName(NULL), "Invalid IP address %s in config, trying to use dhcp", cfg.ip_addr);
			cfg.use_dhcp = 1u;
		}
		if(!inet_aton(cfg.gateway, &gateway))
		{
			syslog_write(LOG_ERROR, pcTaskGetTaskName(NULL), "Invalid gateway address %s in config, trying to use dhcp", cfg.gateway);
			cfg.use_dhcp = 1u;
		}
		if(!inet_aton(cfg.netmask, &netmask))
		{
			syslog_write(LOG_ERROR, pcTaskGetTaskName(NULL), "Invalid netmask %s in config, trying to use dhcp", cfg.netmask);
			cfg.use_dhcp = 1u;
		}
	}

	netif_set_default(netif_add(&net_iface, &ip_address, &netmask, &gateway, NULL, ethernetif_init, tcpip_input));

	netif_set_status_callback(&net_iface, iface_callback);

//	netif_set_link_callback(&xNetIf, lwip_link_callback);

	if(cfg.use_dhcp)
	{
		dhcp_start(&net_iface);
		syslog_write(LOG_INFORMATION, pcTaskGetTaskName(NULL), "Starting DHCP");
	}
	else
	{
		netif_set_up(&net_iface);
	}

	if(inet_aton(cfg.dns, &dns) && !cfg.use_dhcp)
	{
		ip_addr_t current_dns = dns_getserver(0);
		dns_setserver(0, &dns);
		dns_setserver(1u, &current_dns);
	}
}
