/**
 * @file 	cli_wrappers.c
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Implementation of commands for FreeRTOS+CLI
 */

#include "cli_wrappers.h"

/** Callback for timer to reboot the device */
static void reboot_tmr_callback(xTimerHandle tmr)
{
	NVIC_SystemReset();
}

/* Command function definitions */
static portBASE_TYPE xPS( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
	const char *const header = "Task          State  Priority  Stack	#\r\n************************************************\r\n";

	strcpy(pcWriteBuffer, header);
	vTaskList(pcWriteBuffer + strlen(header));

	return pdFALSE;
}

static portBASE_TYPE xDATE( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
	time_t t = get_time_counter(POSIXTIME_T);
	strcpy(pcWriteBuffer, ctime(&t));

	return pdFALSE;
}

static portBASE_TYPE xUPTIME( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
	get_string_uptime(pcWriteBuffer, xWriteBufferLen);

	return pdFALSE;
}

static portBASE_TYPE xFREE( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
	snprintf(pcWriteBuffer, xWriteBufferLen, "\t\t\t\ttotal\tused\tfree\r\nFreeRTOS Heap:\t%u\t%u\t%u\r\n",
			configTOTAL_HEAP_SIZE, configTOTAL_HEAP_SIZE - xPortGetFreeHeapSize(), xPortGetFreeHeapSize());

	return pdFALSE;
}

static portBASE_TYPE xREBOOT( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
	snprintf(pcWriteBuffer, xWriteBufferLen, "System is going down for reboot now\r\n");
	syslog_write(LOG_WARNING, pcTaskGetTaskName(NULL), "System is going down for reboot now");

	xTimerHandle tmr = xTimerCreate("reboot tmr", 500u / portTICK_RATE_MS, pdFALSE, NULL, reboot_tmr_callback);
	xTimerStart(tmr, 1000u / portTICK_RATE_MS);

	return CLI_WRAPPERS_REBOOT_CLOSE_CONNECTION_RETURN_FLAG;
}

/* Command struct definitions */
static const xCommandLineInput xPSStruct = {
	( const int8_t * const ) "ps",
	( const int8_t * const ) "ps: list all running tasks\r\n",
	xPS,
	0
};

static const xCommandLineInput xDATEStruct = {
	( const int8_t * const ) "date",
	( const int8_t * const ) "date: display current date and time\r\n",
	xDATE,
	0
};

static const xCommandLineInput xUPTIMEStruct = {
	( const int8_t * const ) "uptime",
	( const int8_t * const ) "uptime: time since the system is powered on\r\n",
	xUPTIME,
	0
};

static const xCommandLineInput xFREEStruct = {
	( const int8_t * const ) "free",
	( const int8_t * const ) "free: display free heap memory\r\n",
	xFREE,
	0
};

static const xCommandLineInput xREBOOTStruct = {
	( const int8_t * const ) "reboot",
	( const int8_t * const ) "reboot: just restart the system\r\n",
	xREBOOT,
	0
};


uint_fast8_t cli_wrappers_init(void)
{
	static uint8_t initialized = 0;
	if(!initialized)
	{
		FreeRTOS_CLIRegisterCommand(&xPSStruct);
		FreeRTOS_CLIRegisterCommand(&xUPTIMEStruct);
		FreeRTOS_CLIRegisterCommand(&xFREEStruct);
		FreeRTOS_CLIRegisterCommand(&xREBOOTStruct);

		initialized = 1u;
	}
	return initialized;
}

